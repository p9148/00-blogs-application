from django.db import models


class Blog(models.Model):
    title = models.CharField(
        verbose_name='title',
        max_length=100,
    )
    text = models.TextField(
        verbose_name="text",
        blank=True,
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
        null=True,
    )

    def __str__(self):
        return f'{self.title}'