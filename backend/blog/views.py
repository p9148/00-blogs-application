from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views import View
from blog.models import Blog
import json


class WelcomeView(View):
    def get(self, request):
        return HttpResponse('Hello, welcome to our first Django View!!')


class GetAllBlogs(View):
    def get(self, request):
        blogs = Blog.objects.all()
        response = [{'id': blog.id, 'title': blog.title,
                     'text': blog.text, 'created': blog.created} for blog in blogs]
        return JsonResponse(response, safe=False)


class GetBlog(View):
    def get(self, request, *args, **kwargs):
        blog_id = kwargs.get('blog_id')
        blog = get_object_or_404(Blog, id=blog_id)

        response = {
            'id': blog.id,
            'title': blog.title,
            'text': blog.text,
            'created': blog.created,
        }

        return JsonResponse(response, status=200)


class PostBlog(View):
    def post(self, request, *args, **kwargs):
        if request.body:
            data = json.loads(request.body)
            # blog = Blog.objects.create(
            #     title=data.get('title'),
            #     text = data.get('text')
            # )
            blog = Blog.objects.create(**data)
            response = {
                "id": blog.id,
                "title": blog.title,
                "text": blog.text,
                "created": blog.created
            }

            return JsonResponse(response)


class PatchBlog(View):
    def patch(self, request, *args, **kwargs):
        blog_id = kwargs.get('blog_id')
        blog = get_object_or_404(Blog, id=blog_id)
        data = json.loads(request.body)
        setattr(blog, 'title', data.get('title'))
        setattr(blog, 'text', data.get('text'))
        blog.save()
        response = {
            "id": blog.id,
            "title": blog.title,
            "text": blog.text,
            "created": blog.created
        }

        return JsonResponse(response)


class DeleteBlog(View):
    def delete(self, request, *args, **kwargs):
        blog_id = kwargs.get('blog_id')
        blog = get_object_or_404(Blog, id=blog_id)
        blog.delete()

        return HttpResponse(status=204)
