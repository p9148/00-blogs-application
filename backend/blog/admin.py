from django.contrib import admin
from .models import Blog


class BlogEntryAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'text', 'created']
    search_fields = ['title', 'text']
    list_filter = ['created']


# Register your models here.
# some new line
admin.site.register(Blog, BlogEntryAdmin)
