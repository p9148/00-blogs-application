from django.urls import path
from .views import WelcomeView, GetAllBlogs, PostBlog, GetBlog, PatchBlog, DeleteBlog

urlpatterns = [
    path('blog/', WelcomeView.as_view()),
    path('blog/all/', GetAllBlogs.as_view()),
    path('blog/<int:blog_id>/', GetBlog.as_view()),
    path('blog/new/', PostBlog.as_view()),
    path('blog/update/<int:blog_id>/', PatchBlog.as_view()),
    path('blog/delete/<int:blog_id>/', DeleteBlog.as_view()),
]