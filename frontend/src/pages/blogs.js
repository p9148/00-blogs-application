import React, { Component } from 'react';
import axios from 'axios';
import { useEffect, useState } from 'react';
// import user components and pages
import Blog from '../components/blog';
import Button from '../components/button/button';
import NewBlog from '../components/newBlog/newBlog';

const Blogs = () => {
    const [blogs, setBlogs] = useState([]);
    const [blog, setBlog] = useState({});
    const [hidden, setHidden] = useState(true);
    const [baseUrl, setBaseUrl] = useState('http://127.0.0.1:8001');

    // hooks
    useEffect(() => {
        fetchBlogs();
    }, [])

    // added functionality
    const addNewBlog = () => {
        setBlog({});
        setHidden(false);
    }

    const fetchBlogs = () => {
        setBlogs([]);

        axios.get(`${baseUrl}/blog/all/`)
            .then((response) => {
                console.log(response.data);

                setBlogs(response.data);
            })
    }

    const handleCancelClick = () => {
        setHidden(true);
    }

    const handleDelete = (id) => {
        console.log('deleting item...');

        console.log(id);

        let url = `${baseUrl}/blog/delete/${id}/`

        axios.delete(url)
            .then((response) => {
                console.log(response.data);
                fetchBlogs();
            })
    }

    const handleUpdate = (blog) => {
        console.log('updating blog: ', blog);

        setBlog(blog);
        setHidden(false);
    }

    const handleSaveClick = (blog) => {
        console.log('handleSaveClick');
        console.log(blog.title);
        console.log(blog.text);

        if (blog.id === undefined) {
            console.log('inside: blog.id == undefined ', blog)
            axios.post(`${baseUrl}/blog/new/`, {
                title: blog.title,
                text: blog.text,
            })
                .then((response) => {
                    console.log(response.data);

                    fetchBlogs();
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else {
            axios.patch(`${baseUrl}/blog/update/${blog.id}/`, {
                title: blog.title,
                text: blog.text,
            })
                .then((response) => {
                    console.log(response.data);

                    fetchBlogs();
                })
                .catch((error) => {
                    console.log(error);
                })
        }

        setHidden(true);
    }

    return (
        <div className='bg-gray-200 h-screen'>
            <div id='header' className='w-full bg-gray-400 h-20 flex justify-center items-center'>
                <h1 className='text-xl'>Welcome to simple Blog Editor</h1>
                <Button label={'Add new Blog'} handleClick={addNewBlog} />
            </div>
            <div id='main' className='w-full flex justify-center'>
                {blogs.map((blog) => {
                    return <Blog
                        key={Math.random() * 1000000}
                        id={blog.id}
                        title={blog.title}
                        text={blog.text}
                        created={blog.created}
                        handleDelete={() => handleDelete(blog.id)}
                        handleUpdate={() => handleUpdate(blog)}
                    />
                })}
            </div>
            <NewBlog blog={blog} handleSaveClick={handleSaveClick} handleCancelClick={handleCancelClick} hidden={hidden} />
        </div>);
}

export default Blogs;