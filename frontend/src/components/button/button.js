import React, { Component } from 'react';
import './button.scss';

const Button = (props) => {
    return (
        <div className='button' onClick={props.handleClick}>
            {props.label}
        </div>);
}

export default Button;