import React, { Component } from 'react';
import { useState, useEffect } from 'react';

import Button from '../button/button';
import './newBlog.scss';



const NewBlog = (props) => {
    const [id, setId] = useState('')
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [created, setCreated] = useState('');

    useEffect(() => {
        console.log('NewBlog: component changed...');

        console.log('props: ', props.blog);

        if (props.blog.id !== undefined)
            setId(props.blog.id);
        else
            setId('');

        if (props.blog.title !== undefined)
            setTitle(props.blog.title);
        else
            setTitle('');

        if (props.blog.text !== undefined)
            setText(props.blog.text);
        else
            setText('');

        if (props.blog.created !== undefined)
            setCreated(props.blog.created);
        else
            setCreated('');
    }, [props.blog])

    const saveNewBlog = () => {
        let blog = {
            title: title,
            text: text
        }

        props.handleSaveClick(blog);

        setTitle('');
        setText('');
    }

    const updateBlog = () => {
        let blog = {
            id: props.blog.id,
            title: title,
            text: text,
            created: props.blog.created,
        }

        props.handleSaveClick(blog);

        setId('');
        setTitle('');
        setText('');
        setCreated('');
    }

    return (
        <div className={props.hidden ? 'new-blog-hidden' : 'new-blog'}>
            <div className='new-blog-container flex flex-col justify-between'>
                <div className='flex p-2 justify-between'>
                    <div>title</div>
                    <input className='new-blog-input' type='text' value={title} onChange={(e) => setTitle(e.target.value)} />
                </div>
                <div className='flex p-2 justify-between'>
                    <div>text</div>
                    <textarea className='new-blog-input' rows='5' cols='33' value={text} onChange={(e) => setText(e.target.value)} />
                </div>
                <div className='flex justify-around pb-3'>
                    <Button label={'Save'} handleClick={props.blog.id === undefined ? saveNewBlog : updateBlog} />
                    <Button label={'Cancel'} handleClick={props.handleCancelClick} />
                </div>
            </div>
        </div>);
}

export default NewBlog;