import React, { Component } from 'react';
import Button from './button/button';

const Blog = (props) => {
    return (
        <div className='w-52 h-64 bg-red-300 p-2 m-5 flex flex-col justify-between'>
            <div>
                <p>{props.id}</p>
                <h2 className='w-full flex justify-center text-xl'>{props.title}</h2>
                <p className='text-sm'>{props.text}</p>
            </div>
            <div>
                <p className='text-sm'>{props.created}</p>
                <div className='flex justify-between pt-2'>
                    <Button label={'update'} handleClick={props.handleUpdate} />
                    <Button label={'delete'} handleClick={props.handleDelete} />
                </div>
            </div>
        </div>);
}

export default Blog;